#!/bin/bash

# start nginx in background
nginx -g "daemon on;"

# start postal in foreground
/usr/bin/postal run