FROM ruby:2.3-alpine

#
# test
#

WORKDIR /opt/postal

COPY entrypoint.sh /scripts/
COPY Gemfile /tmp/
COPY postal.yml /tmp/
COPY postal /tmp/

RUN cd /tmp \
  # install dependencies
  && apk add --update --no-cache --virtual .build-deps build-base git openssl libcap \
  && apk add --update --no-cache nodejs mariadb-dev \
  && gem install bundler procodile \
  # add bash
  && apk add --update --no-cache bash \
  # create user + update workdir permissions
  && adduser -D -g "" -s /bin/bash -h /opt/postal postal \
  # ruby config
  && setcap 'cap_net_bind_service=+ep' /usr/local/bin/ruby \
  # copy source
  && su postal -c "mkdir -p /opt/postal/app" \
  && wget https://postal.atech.media/packages/stable/latest.tgz -O - | su postal -c "tar zxpv -C /opt/postal/app" \
  && ln -s /opt/postal/app/bin/postal /usr/bin/postal \
  # add config files, postal bundle
  && mkdir /opt/postal/config \
  && chown postal:postal /opt/postal/config \
  && mv /tmp/postal.yml /opt/postal/config/postal.yml \
  && mv /tmp/Gemfile /opt/postal/app/Gemfile \
  && mv /tmp/postal /opt/postal/app/bin/postal \
  && cp /opt/postal/app/spec/config/lets_encrypt.pem /opt/postal/config/lets_encrypt.pem \
  && cp /opt/postal/app/spec/config/signing.key /opt/postal/config/signing.key \
  && /opt/postal/app/bin/postal bundle /opt/postal/vendor/bundle \
  # setup nginx
  && apk add --update --no-cache nginx \
  && mkdir /run/nginx \
  && mkdir /etc/nginx/ssl/ \
  && openssl req -x509 -newkey rsa:4096 -keyout /etc/nginx/ssl/postal.key -out /etc/nginx/ssl/postal.cert -days 365 -nodes -subj "/C=GB/ST=Example/L=Example/O=Example/CN=example.com" \
  # remove
  && apk del .build-deps

ENTRYPOINT ["/scripts/entrypoint.sh"]